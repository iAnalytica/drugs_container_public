# -*- coding: utf-8 -*-
"""
Created on Fri Dec 28 17:26:59 2018

@author: ilaponog
"""
count = -1;
for gene_compound_threshold in [0, 100, 200, 300, 400, 500, 600, 700]:
    for inchikey_matching in ['strict', 'auto', 'soft']:
        for stitch_matching in ['stereo', 'stereoflat', 'all']:
            for pagerank_c in [0.002, 0.004, 0.01, 0.03, 0.05, 0.076, 0.1, 0.2]:
                for bioplex in [0, 300, 400, 500, 600, 700, 800]:
                    for outliers in ['True', 'False']:
                        for method in ['MMC', 'LinearSVM']:
                            for log_transform in ['True', 'False']:
                                count += 1;
                                with open('e:/Linux/Exchange/Vodafone/Container/Cluster/settings_%s.ini'%count,'w') as fout:
                                    fout.write('[Default Paths]\n');
                                    fout.write('Compounds_Path = ./Compounds\n');
                                    fout.write('Interactome_Path = ./Interactome\n');
                                    fout.write('Results_Path = ./Results\n');
                                    fout.write('Scratch_Path = ./Scratch\n');
                                    fout.write('[Preparation]\n');
                                    fout.write('InChIKey_matching = %s\n'%inchikey_matching);
                                    fout.write('STITCH_ID_selection = %s\n'%stitch_matching);
                                    fout.write('[Network Propagation Settings]\n');
                                    fout.write('gene_compound_threshold = %s\n'%gene_compound_threshold);
                                    fout.write('STRING_threshold = %s\n'%bioplex);
                                    if bioplex == 0:
                                        fout.write('BioPlex_threshold = 1\n');
                                    else:
                                        fout.write('BioPlex_threshold = 0\n');
                                    fout.write('PageRank_c = %s\n'%pagerank_c);
                                    fout.write('[Machine Learning Settings]\n');
                                    fout.write('Remove_Outliers = %s\n'%outliers);
                                    fout.write('Method = %s\n'%method);
                                    fout.write('LogTransform = %s\n'%log_transform);
                                    fout.write('N_repeats = 10\n');
                                    fout.write('CV_folds = 5\n');
                                    fout.write('Test_size = 0.2\n');
                                    fout.write('[Cleanup]\n');
                                    fout.write('cleanup_enabled = False\n');
