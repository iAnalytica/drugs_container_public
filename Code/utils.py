# -*- coding: utf-8 -*-
"""
DRUGS Project module: utils.py
    
Collection of functions and routines for other modules to load, map and pre-process data.    

Created on Wed Dec  5 15:47:37 2018

--------------------------------------------------------------------------
Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2019 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
import numpy as np;
import os;
import sys;
import hashlib;
import configparser;
import gzip;

def escapeit(s):
    if isinstance(s, frozenset) or isinstance(s, set) or isinstance(s, list):
        s = list(s);
        for i in range(len(s)):
            if not isinstance(s[i], str):
                s[i] = str(s[i]);
            if '|' in s[i]:
                s[i] = '"%s"'%s[i];
        s = '|'.join(s);
    if not isinstance(s, str):    
        s = str(s);
    s=s.encode('ascii','ignore');
    s=s.replace("\xA0"," ").replace("\\","\\\\").replace("'","\\'").replace("\n","\\n").replace("\"","\\\"");
    return s;    

def get_sha256_dig(inchi):
    s = bytearray(hashlib.sha256(inchi.encode()).digest());
    val=np.array(s,dtype=np.uint8);
    return val;

def write_signed_int16(out, value):
    out.extend(np.int16(value).tobytes());

def write_num(out, number, l_address_size = False):
    if l_address_size:
        out.extend(np.int32(number).tobytes());
    else:
        out.extend(np.int16(number).tobytes());
        
        
def write_float(out, value):
    out.extend(np.float32(value).tobytes());

    
def join_and_map_compounds(compounds_1, compounds_0, compounds_test, stitch_file, stitch_selection, stitch_threshold, report_path, match_type):
    #total_compounds = {'Genes':[], 'Indeces':[], 'Group':[], 'IDs':[], 'Name':[], 'InChIKey':[]};
    total_compounds = {};
    total_compounds['IDs'] = list(compounds_1.index) + list(compounds_0.index) + list(compounds_test.index);
    total_compounds['Name'] = list(compounds_1['Name']) + list(compounds_0['Name']) + list(compounds_test['Name']);
    total_compounds['InChIKey'] = list(compounds_1['InChIKey']) + list(compounds_0['InChIKey']) + list(compounds_test['InChIKey']);
    total_compounds['Group'] = [1] * len(compounds_1.index) + [0]* len(compounds_0.index) + [-1] * len(compounds_test.index);
    total_compounds['Indeces'] = list(range(len(total_compounds['IDs'])));
    total_compounds['Genes'] = [0] * len(total_compounds['IDs']);
    total_compounds['Genes_soft'] = [0] * len(total_compounds['IDs']);
    
    inchi_to_index = {};
    short_inchi_to_index = {};
    
    for i in range(len(total_compounds['InChIKey'])):
        inchikey = total_compounds['InChIKey'][i];
        if inchikey in inchi_to_index:
            inchi_to_index[inchikey].append(i);
        else:
            inchi_to_index[inchikey] = [i];

        short_inchikey = inchikey.split('-')[0];
            
        if short_inchikey in short_inchi_to_index:
            short_inchi_to_index[short_inchikey].append(i);
        else:
            short_inchi_to_index[short_inchikey] = [i];

    if stitch_selection == 'all':
        selmask = 1;
    elif stitch_selection == 'flat':
        selmask = 2;
    elif stitch_selection == 'stereoflat':
        selmask = 4;
    else:
        selmask = 8;
    
    if sys.version_info[0] == 2:
        opening_mode = 'r';
    else:
        opening_mode = 'rt';
    
    with gzip.open(stitch_file, opening_mode) as finp:
        i = -1;
        for s in finp:
            i += 1;
            if i % 10000 == 0:
                print(i);
            if i > 0:
                s = s.rstrip().split(',');
                inchikey = s[1].lstrip('"').rstrip('"');
            
                if inchikey in inchi_to_index:
                    selected_genes = set();
                    genes = s[2].lstrip('"').rstrip('"').split('|');
                    for gene_entry in genes:
                        gene_id, gene_score, gene_mask = gene_entry.split(':');
                        if (int(gene_score) >= stitch_threshold) and (int(gene_mask) & selmask > 0):
                            selected_genes.add(int(gene_id));
                    if len(selected_genes) > 0:
                        for index in inchi_to_index[inchikey]:
                            if isinstance(total_compounds['Genes'][index], set):
                                total_compounds['Genes'][index] = total_compounds['Genes'][index].union(selected_genes);
                            else:
                                total_compounds['Genes'][index] = selected_genes;
                
                short_inchikey = inchikey.split('-')[0];                
                                
                if short_inchikey in short_inchi_to_index:
                    selected_genes = set();
                    genes = s[2].lstrip('"').rstrip('"').split('|');
                    for gene_entry in genes:
                        gene_id, gene_score, gene_mask = gene_entry.split(':');
                        if (int(gene_score) >= stitch_threshold) and (int(gene_mask) & selmask > 0):
                            selected_genes.add(int(gene_id));
                    if len(selected_genes) > 0:
                        for index in short_inchi_to_index[short_inchikey]:
                            if isinstance(total_compounds['Genes_soft'][index], set):
                                total_compounds['Genes_soft'][index] = total_compounds['Genes_soft'][index].union(selected_genes);
                            else:
                                total_compounds['Genes_soft'][index] = selected_genes;
                                
    if match_type == 'soft':
        total_compounds['Genes'] = total_compounds['Genes_soft'];
    elif match_type == 'auto':
        for i in range(len(total_compounds['IDs'])):
            if not isinstance(total_compounds['Genes'][i], set):
                total_compounds['Genes'][i] = total_compounds['Genes_soft'][i];
    
    del total_compounds['Genes_soft'];

    with open(os.path.join(report_path, 'compounds_with_no_gene_connections.csv'), 'w') as fout:
        fout.write('Index,ID,Name,InChIKey,Group\n');
        
        for i in reversed(range(len(total_compounds['IDs']))):
            if not isinstance(total_compounds['Genes'][i], set):
                fout.write(','.join([
                        '%s'%total_compounds['Indeces'][i],
                        '"%s"'%total_compounds['IDs'][i],
                        '"%s"'%total_compounds['Name'][i],
                        '"%s"'%total_compounds['InChIKey'][i],
                        '%s'%total_compounds['Group'][i],
                        ]))
                fout.write('\n');

                del total_compounds['Genes'][i];
                del total_compounds['IDs'][i];
                del total_compounds['Name'][i];
                del total_compounds['InChIKey'][i];
                del total_compounds['Group'][i];
                del total_compounds['Indeces'][i];
        
    
    return total_compounds;
    
def load_global_settings(settings_file):
    global_settings = configparser.ConfigParser();
    global_settings.read(settings_file);
        
    for subpath in ['Compounds_Path',
                    'Interactome_Path',
                    'Results_Path',
                    'Scratch_Path',
                    ]:
        if not os.path.isabs(global_settings['Default Paths'][subpath]):
            global_settings['Default Paths'][subpath] = os.path.abspath(os.path.join(os.path.dirname(settings_file), global_settings['Default Paths'][subpath]));
            
    return global_settings
    
    
    