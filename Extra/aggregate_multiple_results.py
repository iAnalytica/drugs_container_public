# -*- coding: utf-8 -*-
"""
Created on Wed Jan  2 14:05:38 2019

@author: ilaponog
"""
import os
import tarfile
import sys;
import numpy as np;
from shutil import copyfile
import pandas as pd


#Checking if the system is right and if the module is run as a script instead of being imported
if __name__== '__main__':
    if sys.byteorder!='little':
        print('Only little endian machines currently supported! bye bye ....');
        quit();
else:
    print('This module is not meant to be imported, but should be run as a script instead!')
    quit();
    
#Figuring out and adding the path to local modules    
module_path = os.path.dirname(os.path.realpath(__file__));
sys.path.append(module_path);

from utils import load_global_settings; 
from correlate import correlate_all

in_paths = [
        ('e:/Linux/Exchange/Vodafone/Container/Cluster/',
         'g:/FTP_Internal/TestContainer/MultiResults/'),
        ('e:/Linux/Exchange/Vodafone/Container/Cluster2/',
         'g:/FTP_Internal/TestContainer/MultiResults2/'),
        ('e:/Linux/Exchange/Vodafone/Container/Cluster3/',
         'g:/FTP_Internal/TestContainer/MultiResults3/'),
        
        ];
        
        
in_files = [];

def get_files_list(path = os.getcwd(), recursive = True, extensions = set(['.*'])):
    result = [];
    if recursive:
        result = [os.path.join(dp, f) for dp, dn, filenames in os.walk(path) for f in filenames if (os.path.splitext(f)[1] in extensions) or ('.*' in extensions)]
    else:
        result = [os.path.join(path, f) for f in os.listdir(path) if (os.path.isfile(os.path.join(path, f))) and ((os.path.splitext(f)[1] in extensions) or ('.*' in extensions))];
    return result;


for paths in in_paths:
    results = set(get_files_list(paths[1], recursive = False, extensions = set(['.gz'])));
    for result in results:
        fname = os.path.basename(result);
        fname = fname.split('_')[1].replace('.tar.gz', '');
        settings_name = os.path.join(paths[0], 'settings_%s.ini'%fname);
        if os.path.isfile(settings_name):
            in_files.append((settings_name, result))

summary = [];
scores = [];
gene_scores = [];
drug_scores = {};
compound_scores = {};

subi = -1;

ref1 = pd.read_csv('e:/Linux/Exchange/Vodafone/Container/Compounds/1.csv.gz', index_col = 0);
ref0 = pd.read_csv('e:/Linux/Exchange/Vodafone/Container/Compounds/0.csv.gz', index_col = 0);
cmpref = pd.read_csv('e:/Linux/Exchange/Vodafone/Container/Compounds/test.csv.gz', index_col = 0);

for index in ref1.index:
    drug_scores[index] = np.full((700,), -1.0, dtype = np.float32);

for index in ref0.index:
    drug_scores[index] = np.full((700,), -1.0, dtype = np.float32);

for index in cmpref.index:
    compound_scores[index] = np.full((700,), -1.0, dtype = np.float32);

for result_index in range(len(in_files)):
    file_set = in_files[result_index];
    print('%s of %s'%(result_index + 1, len(in_files)))
    settings = load_global_settings(file_set[0]);
    result = {};        
    result['Settings'] = file_set[0];

    result['InChI_matching'] = settings['Preparation']['InChIKey_matching'];
    result['STITCH_ID'] = settings['Preparation']['STITCH_ID_selection'];
    result['Gene_Compound_threshold'] = settings['Network Propagation Settings']['gene_compound_threshold'];
    result['Gene_Gene_STRING_threshold'] = settings['Network Propagation Settings']['STRING_threshold'];
    result['Gene_Gene_BioPlex_threshold'] = settings['Network Propagation Settings']['BioPlex_threshold'];
    result['PageRank_c'] = settings['Network Propagation Settings']['PageRank_c'];
    result['Remove_Outliers'] = settings['Machine Learning Settings']['Remove_Outliers'];
    result['Method'] = settings['Machine Learning Settings']['Method'];
    result['LogTransform'] = settings['Machine Learning Settings']['LogTransform'];
    
    f_score = -1;
    with tarfile.open(file_set[1]) as results:
        subnames_list = results.getnames();
        for i in range(len(subnames_list)):
            if subnames_list[i].endswith('_params_optimization.csv'):
                rfile = results.extractfile(subnames_list[i]);
                for s in rfile:
                    s = s.rstrip();
                    if s.startswith('post_f_score'):
                        f_score = float(s.split(' ')[1]);
                        scores.append(f_score);
                        result['F-score'] = f_score;
                        result['Std(F-score)'] = s.split(' ')[2].replace('(', '').replace(')', '');
                    elif s.startswith('post_ac_score'):    
                        result['AC_correct'] = s.split(' ')[1];
                        result['Std(AC_correct)'] = s.split(' ')[2].replace('(', '').replace(')', '');
                    elif s.startswith('post_nac_score'):    
                        result['non_AC_correct'] = s.split(' ')[1];
                        result['Std(non_AC_correct)'] = s.split(' ')[2].replace('(', '').replace(')', '');
                    elif s.startswith('post_logratio'):    
                        result['LogRatio'] = s.split(' ')[1];
                        result['Std(LogRatio)'] = s.split(' ')[2].replace('(', '').replace(')', '');
            elif subnames_list[i].endswith('_outliers_stats.csv'):             
                rfile = results.extractfile(subnames_list[i]);
                ii = -1;
                for s in rfile:
                    ii += 1;
                    if ii == 1:
                        values = s.rstrip().split(',');
                        result['Train AC outliers'] = values[0];
                        result['Train NAC outliers'] = values[1];
                        result['Train AC count'] = values[2];
                        result['Train NAC count'] = values[3];
                        result['Train AC outliers fraction'] = values[4];
                        result['Train NAC outliers fraction'] = values[5];
                        result['Test outliers'] = values[6];
                        result['Test count'] = values[7];
                        result['Test outliers fraction'] = values[8];

        if f_score >= 0.83999:
            
            subi += 1;
            for i in range(len(subnames_list)):
                if subnames_list[i].endswith('_gene_scores.csv'):
                    rfile = results.extractfile(subnames_list[i]);
                    gs = pd.read_csv(rfile, index_col = 0);
                    gscores = np.array(gs['Gene_Score'], dtype = np.float32);
                    print(gscores.shape);
                    gene_scores.append(gscores);
                elif subnames_list[i].endswith('_prob_train.csv'):
                    rfile = results.extractfile(subnames_list[i]);
                    pr = pd.read_csv(rfile, index_col = 1);
                    for index in pr.index:
                        cid = pr[' ID'][index];
                        prob = pr['AC probability'][index];
                        drug_scores[cid][subi] = prob;

                elif subnames_list[i].endswith('_prob_test.csv'):
                    rfile = results.extractfile(subnames_list[i]);
                    pr = pd.read_csv(rfile, index_col = 1);
                    for index in pr.index:
                        cid = pr[' ID'][index];
                        prob = pr['AC probability'][index];
                        compound_scores[cid][subi] = prob;
                    
                    


    if f_score > 0:
        summary.append(result);
    
    
scores = np.array(scores);

indexes = np.argsort(-scores);
scores = scores[indexes];

assert(len(scores) == len(summary))

sorted_summary = [summary[i] for i in indexes];

headers = set();
for value in summary:
    headers = headers.union(set(value.keys()));

with open('g:/FTP_Internal/TestContainer/full_results.csv', 'w') as fout:
    fout.write('Index,');
    fout.write(','.join(list(headers)));
    fout.write('\n');
    for i in range(len(sorted_summary)):
        fout.write('%s'%i);
        result = sorted_summary[i];
        for header in headers:
            if header in result:
                value = result[header];
            else:
                value = '-';
            fout.write(',"%s"'%value);
        fout.write('\n');

print('Finished!')
#print(scores)
#%%
jj = -1;
for i in range(len(sorted_summary)):
    if sorted_summary[i]['F-score'] >= 0.83999 and sorted_summary[i]['F-score'] < 0.849:
        jj += 1;
        copyfile(sorted_summary[i]['Settings'], 'e:/Linux/Exchange/Vodafone/Container/Cluster_best2/settings_%s.ini'%jj);
        

#%%
with open('g:/FTP_Internal/TestContainer/drugs.csv', 'w') as fout:        
    fout.write('Primary_ID,AC_probability,Count\n');
    for key in drug_scores:
        fout.write(','.join([
                '%s'%key,
                '%.4f'%np.mean(drug_scores[key][drug_scores[key] >= 0.0]),
                '%s'%np.sum(drug_scores[key] >= 0.0),
                ]));
        fout.write('\n');
#%%
with open('g:/FTP_Internal/TestContainer/compounds.csv', 'w') as fout:        
    fout.write('Primary_ID,AC_probability,Count\n');
    for key in compound_scores:
        fout.write(','.join([
                '%s'%key,
                '%.4f'%np.mean(compound_scores[key][compound_scores[key] >= 0.0]),
                '%s'%np.sum(compound_scores[key] >= 0.0),
                ]));
        fout.write('\n');
        
#%%
gene_scores = np.vstack(gene_scores);

#%%
nons = np.isnan(gene_scores)
nf = np.any(nons, axis = 0)
#%%
gs_filtered = gene_scores[:, np.logical_not(nf)];


#%%
      
corrs = correlate_all(gs_filtered, gs_filtered);
#%%
mean_gene_scores = np.mean(gene_scores, axis = 0)
std_gene_scores = np.std(gene_scores, axis = 0)
    
#%%
gs_new = gs.drop(columns = ['Gene_Score']);
gs_new.insert(1, 'Mean Score', 0.0);
gs_new.insert(2, 'Std Score', 0.0);

for i in range(20256):
    if i %100 ==0:
        print(i)
    gs_new['Mean Score'][i] = mean_gene_scores[i];
    gs_new['Std Score'][i] = std_gene_scores[i];

#%%
gs_new.to_csv('g:/FTP_Internal/TestContainer/all_gene_scores.csv')
#%%
ii = -1;
used_ids = set();
with open('g:/FTP_Internal/TestContainer/food_ids.csv', 'r') as finp:
    with open('g:/FTP_Internal/TestContainer/food_ids_with_prediction.csv', 'w') as fout:
        for s in finp:
            ii += 1;
            if ii == 0:
                fout.write('ID,Name,AC score,Model count\n');
            else:
                s = s.rstrip().split(',', 1);
                idvalue = s[0];
                used_ids.add(idvalue);
                namevalue = s[1];
                if idvalue in compound_scores:
                    ac_score = np.mean(compound_scores[idvalue][compound_scores[idvalue] >= 0.0]);
                    model_count = np.sum(compound_scores[idvalue] >= 0.0);
                else:
                    ac_score = -1.0;
                    model_count = 0;
                fout.write(','.join([
                        '%s'%idvalue,
                        '%s'%namevalue,
                        '%.4f'%ac_score,
                        '%s'%model_count,
                        ]));
                fout.write('\n');
    
#%%        
with open('g:/FTP_Internal/TestContainer/food_ids_with_prediction_not_in_list.csv', 'w') as fout:
    for idvalue in compound_scores:
        if not idvalue in used_ids:
            ac_score = np.mean(compound_scores[idvalue][compound_scores[idvalue] >= 0.0]);
            model_count = np.sum(compound_scores[idvalue] >= 0.0);
            
            fout.write(','.join([
                        '%s'%idvalue,
                        '"%s"'%cmpref['Name'][idvalue],
                        '%.4f'%ac_score,
                        '%s'%model_count,
                        ]));
            fout.write('\n');
            



        