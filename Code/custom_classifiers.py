# -*- coding: utf-8 -*-
"""
DRUGS Project module: custom_classifiers.py

Created on Tue Oct 23 15:11:22 2018

Implementations of LinearSVM and MMC methods with built-in logistic regression
for probability estimation.


--------------------------------------------------------------------------

Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2019 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""


import numpy as np;
from numpy.linalg import eig;
from sklearn.decomposition import PCA;
from sklearn.linear_model import LogisticRegression;
from sklearn.svm import LinearSVC;

class LinearSVC_with_probability(LinearSVC):
        
    def fit(self, X, y, sample_weight=None):
        LinearSVC.fit(self, X = X, y = y, sample_weight = sample_weight);

        self.T_ = np.dot(X, np.transpose(self.coef_));

        self.logreg_estimator_ = LogisticRegression(class_weight = 'balanced').fit(self.T_.reshape(-1, 1), y);

        resval = self.logreg_estimator_.predict_proba(self.T_.reshape(-1, 1));

        self.ii_ = np.where(self.logreg_estimator_.classes_ == self.classes_[1])[0][0];

        resval = resval[:, self.ii_];

        result = np.zeros(shape = (X.shape[0], ), dtype = self.classes_.dtype);
        result[resval >= 0.5] = self.classes_[1];
        result[resval < 0.5] = self.classes_[0];

        self.ratio_ = np.float64(np.sum(result == y)) / X.shape[0];
        
        return self
         
    def predict(self, X):
        resval = self.predict_proba(X);
        resval = resval[:, self.ii_];

        result = np.zeros(shape = (X.shape[0], ), dtype = self.classes_.dtype);
        result[resval >= 0.5] = self.classes_[1];
        result[resval < 0.5] = self.classes_[0];

        return result;
        
    def predict_proba(self, X):
        T = np.dot(X, np.transpose(self.coef_));
        return self.logreg_estimator_.predict_proba(T.reshape(-1, 1));




class MMC_Classifier_PCA:
    def __init__(self, PCA_threshold = 0.0, **kwargs):
        self.PCA_threshold_ = PCA_threshold;
    
    def predict_proba(self, X):
        X = self.pca_.transform(X);
        T = np.dot(X, self.coef_);
        return self.logreg_estimator_.predict_proba(T.reshape(-1, 1));
        
        
    def predict(self, X):
        resval = self.predict_proba(X);
        resval = resval[:, self.ii_];
        result = np.zeros(shape = (X.shape[0], ), dtype = self.classes_.dtype);
        result[resval >= 0.5] = self.classes_[1];
        result[resval < 0.5] = self.classes_[0];
        return result;
    
    def fit(self, X, y):
        
        nObs, nVar = X.shape;
        
        classLabel  = np.unique(y);
        
        nClasses    = len(classLabel);
        self.classes_ = classLabel;
        
        #W1, T1    = pcasvd(X, nObs - 1);
        
        if self.PCA_threshold_ != 0.0:
            pca  = PCA();
            pca.fit(X);
            exp_var = 1.0 - self.PCA_threshold_;
            cum_sum = np.cumsum(pca.explained_variance_ratio_);
            n_components = np.sum(cum_sum <= exp_var);
            self.pca_ = PCA(n_components = n_components);
        else:
            self.pca_ = PCA();
        X = self.pca_.fit_transform(X);
        
        nVar        = X.shape[1];

        sampleMean = np.mean(X, axis = 0);
        temp        = np.zeros(shape = (nVar, nVar));
        
        self.class_counts_ = np.zeros((nClasses, ), dtype = np.int32);
        
        for i in range(nClasses):
            indices   = y == classLabel[i];
            self.class_counts_[i] = np.sum(indices);
            classMean = np.mean(X[indices, :], axis = 0).reshape(1, -1);
            temp     +=  np.multiply(np.sum(indices), np.dot(np.transpose(classMean), classMean));
        Sw           = np.dot(np.transpose(X), X) - temp; # within class variability
        Sb           = temp - np.multiply(nObs, np.dot(np.transpose(sampleMean), sampleMean));# % between class variability 
        Sw           = (Sw + np.transpose(Sw)) / 2.0;
        Sb           = (Sb + np.transpose(Sb)) / 2.0;
        D, W         = eig(Sb-Sw);
        indcs = np.argsort(-D);
        W            = W[:, indcs[0]];

        self.coef_ = W;
        self.intercept_ = 0.0;

        self.T_ = np.dot(X, self.coef_);
        
        self.logreg_estimator_ = LogisticRegression(class_weight = 'balanced').fit(self.T_.reshape(-1, 1), y);
        
        resval = self.logreg_estimator_.predict_proba(self.T_.reshape(-1, 1));
        
        self.ii_ = np.where(self.logreg_estimator_.classes_ == self.classes_[1])[0][0];
        resval = resval[:, self.ii_];
        
        result = np.zeros(shape = (X.shape[0], ), dtype = self.classes_.dtype);
        result[resval >= 0.5] = self.classes_[1];
        result[resval < 0.5] = self.classes_[0];
        
        self.ratio_ = np.float64(np.sum(result == y)) / nObs;

        return self


