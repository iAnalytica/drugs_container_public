/*  run_test.cpp

This is a basic test module to run drugs_process and supply it with file names from the command line parameters.

Any questions - please contact Dr. Ivan Laponogov, i.laponogov@imperial.ac.uk

Project DRUGS for Vodafone. 2017

Author: Dr. Ivan Laponogov
Supervisor: Dr. Kirill A. Veselkov


Copyright 2018 Imperial College London

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


*/
#include <iostream>
using namespace std;

#include <drugs_process.h>

int main (int argc, char** argv) {
    //Check it is compiled with 32bit int....
    int intsize = sizeof(int);
    if (intsize < 4) {
        cout << "\nError! Int is smaller than 4 bytes! Recompile with at least 32bit native architecture...\n";
        return -1;

    } 

    int floatsize = sizeof(float);
    if (floatsize != 4) {
        cout << "\nError! float size is not 4 bytes! ...\n";
        return -1;

    } 


    cout << "You have entered " << argc
         << " arguments:" << "\n";

    if (argc != 4) {
        cout <<"Not enough parameters! Supply set A and set B  and output!\n";
        return -1;
    };
    
    char *module_name = argv[0];
    char *set_A = argv[1];
    char *set_B = argv[2];
    char *out_file = argv[3];

    cout <<"Running module name: " << module_name << "\n";
    cout <<"set A: " << set_A << "\n";
    cout <<"set B: " << set_B << "\n";
    cout <<"Output: " << out_file << "\n";

    int result = process_datasets(set_A, set_B, out_file);

    cout <<"Finished\n";

    return result;
}