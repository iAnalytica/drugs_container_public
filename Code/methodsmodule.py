# -*- coding: utf-8 -*-
"""
DRUGS Project module: methodsmodule.py
    
Created on Wed Oct 31 12:32:56 2018

Collection of functions and routines used by Machine learning module. Kept as a 
separate module for clarity and re-usability.

--------------------------------------------------------------------------

Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2019 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
import numpy as np;
from printlog import printlog;
import os;
from outlier_detection import OutlierDetector;

from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from custom_classifiers import MMC_Classifier_PCA, LinearSVC_with_probability;
from sklearn.decomposition import PCA;
from sklearn.metrics import make_scorer
from sklearn.model_selection import StratifiedKFold;
from correlate import correlate_all

import matplotlib.pyplot as plt

def get_list_by_mask(values, mask):
    result = [];
    for i in range(len(mask)):
        if mask[i]:
            result.append(values[i]);
            
    return result
    
    

def get_gene_scores(method, grid, X, y):
    
    X_trans = grid.best_estimator_.steps[0][1].transform(X); #StandardScaler
    
    if method == 0: #Linear SVM - optimize C only
        gene_importance = correlate_all(X_trans.transpose(), grid.best_estimator_.steps[2][1].T_.transpose());
        if grid.best_estimator_.steps[2][1].logreg_estimator_.coef_[0][0] < 0.0:
            gene_importance = -gene_importance;
    elif method == 1: #Radial SVM - optimize C and gamma

        # This is an experimental section, not final!!!!!!!!
        X = X_trans;
        svc = SVC(class_weight = 'balanced', probability = True,
                  C = grid.best_estimator_.steps[2][1].C, gamma = grid.best_estimator_.steps[2][1].gamma);
        
        
        svc.fit(X, y);
        gamma = svc.gamma;
        vectors = svc.support_vectors_;
        coefs = svc.dual_coef_[0, :];

        vv = X.dot(vectors.transpose()) * 2 * gamma;
        vv2 = np.dot(X, X.transpose()).diagonal() * (-gamma);
        vv3 = np.dot(vectors, vectors.transpose()).diagonal() * (-gamma);
        XX = np.exp(np.add(np.add(vv, vv2.reshape(-1,1)), vv3));
        
        g = np.zeros(X.shape);
        bg = np.zeros(X.shape);
    
        for i in range(X.shape[0]):
            xx = XX[i, :];
            x = X[i, :];
            d = vectors - x;
            z = np.multiply(xx.reshape(-1,1), d) * 2.0 * gamma;
            oo = np.dot(z.transpose(), coefs)
            g[i, :] = np.multiply(oo, 1);
            bg[i, :] = np.multiply(oo, x);

        ss = (np.mean(g, axis = 0) +    
              np.mean(bg, axis = 0));
        
        gene_importance = ss/np.sum(ss);
        
    else: #MMC is parameter-less classifier
        gene_importance = correlate_all(X_trans.transpose(), grid.best_estimator_.steps[1][1].T_.transpose());
        if grid.best_estimator_.steps[1][1].logreg_estimator_.coef_[0][0] < 0.0:
            gene_importance = -gene_importance;
    return gene_importance;


def save_predicted_probabilities(fname, prob, ids, names, DBIDs, InChIKeys):
    indeces = np.argsort(-prob);
    with open(fname, 'w') as fout:
        fout.write('AC probability, index, ID, Name, InChIKey\n');
        for i in indeces:
            fout.write('%.3f,%s,"%s","%s","%s"\n'%(prob[i], ids[i], DBIDs[i], names[i], InChIKeys[i]));


def outliers_list(fname, ids, names, DBIDs, InChIKeys):
    with open(fname, 'w') as fout:
        fout.write('index, ID, Name, InChIKey\n');
        for i in range(len(ids)):
            fout.write('%s,"%s","%s","%s"\n'%(ids[i], DBIDs[i], names[i], InChIKeys[i]));


def get_class_prob(estimator, y, class_id):
    index = np.where(estimator.classes_ == class_id)[0][0];
    return y[:, index];

def get_score_average(cv_results, score):
    index = 0;
    accum = [];
    key = 'split%s_test_%s'%(index, score);
    while key in cv_results:
        accum.append(cv_results[key][0]);
        index += 1;
        key = 'split%s_test_%s'%(index, score);
        
    if index > 0:
        accum = np.array(accum, dtype = np.float64);
        return np.mean(accum), np.std(accum);
    else:
        return 0.0, 0.0

def class_balancer(ground_truth, predictions):
    '''
    F-score measure for maximum class balance
    '''
    nac = ground_truth == 0;
    ac = ground_truth == 1;
            
    ac_predictions = predictions[ac] == 1;
    nac_predictions = predictions[nac] == 0;
    v1 = np.float32(np.sum(ac_predictions))/len(ac_predictions);
    v2 = np.float32(np.sum(nac_predictions))/len(nac_predictions);
        
    return 2 * v1 * v2 / (v1 + v2);

def balance_log_ratio(ground_truth, predictions):
    nac = ground_truth == 0;
    ac = ground_truth == 1;
            
    ac_predictions = predictions[ac] == 1;
    nac_predictions = predictions[nac] == 0;
    v1 = np.float32(np.sum(ac_predictions))/len(ac_predictions);
    v2 = np.float32(np.sum(nac_predictions))/len(nac_predictions);
        
    return np.abs(np.log(v1/v2));
    
def ac_correct(ground_truth, predictions):
    '''
    '''
    ac = ground_truth == 1;
    ac_predictions = predictions[ac] == 1;
    v1 = np.float32(np.sum(ac_predictions))/len(ac_predictions);
    return v1;
    
def nac_correct(ground_truth, predictions):
    '''
    '''
    nac = ground_truth == 0;
            
    nac_predictions = predictions[nac] == 0;
    v2 = np.float32(np.sum(nac_predictions))/len(nac_predictions);
        
    return v2;


def load_full_profiles(compounds, vectors, use_log_transform = False):

    vectors = vectors * 1000000;
    
    test = compounds.loc[compounds['Group'] == -1];
    train = compounds.loc[compounds['Group'] != -1];
    
    mask = np.array(compounds['Group'] == -1);
    
    X_train = vectors[np.logical_not(mask), :];
    X_test = vectors[mask, :];
    y_train = np.array(train['Group'], dtype = np.int32);
    train_ids = np.array(train.index, dtype = np.int32);
    train_names = list(train['Name']);
    train_DBIDs = list(train['ID'])
    train_InChIKeys = list(train['InChIKey']);

    test_ids = np.array(test.index, dtype = np.int32);
    test_names = list(test['Name']);
    test_DBIDs = list(test['ID'])
    test_InChIKeys = list(test['InChIKey']);

    if use_log_transform:
        median_X = np.median(X_train);
        if median_X < 1.0:
            median_X = 1.0;
        X_train = np.log(X_train + median_X);
        X_test = np.log(X_test + median_X);
    
    return (X_train, y_train, train_ids, train_names, train_DBIDs, train_InChIKeys,
            X_test, test_ids, test_names, test_DBIDs, test_InChIKeys);
    
    

def remove_outliers(results_path, basefile, \
            X_train, y_train, train_ids, train_names, train_DBIDs, train_InChIKeys,\
            X_test, test_ids, test_names, test_DBIDs, test_InChIKeys):
    
    outlier_remover = OutlierDetector(PCA_threshold = 0.0, contamination = 0.05, cut_off = 0.99);
    outlier_remover.fit(X_train);

    out_train = outlier_remover.predict(X_train);
    XY_train = outlier_remover.X_[:, :2];
    out_test = outlier_remover.predict(X_test);
    XY_test = outlier_remover.X_[:, :2];
    
    min_X = np.min(XY_train[:, 0])
    min_Y = np.min(XY_train[:, 1])
    max_X = np.max(XY_train[:, 0])
    max_Y = np.max(XY_train[:, 1])
    
    min_X = min(min_X, np.min(XY_test[:, 0]))
    min_Y = min(min_Y, np.min(XY_test[:, 1]))
    max_X = max(max_X, np.max(XY_test[:, 0]))
    max_Y = max(max_Y, np.max(XY_test[:, 1]))
    
    
    plt.figure(figsize=(6, 12))
    plt.subplots_adjust(left=.02, right=.98, bottom=.001, top=.96, wspace=.05,
                    hspace=.4)

    colors = np.array(['#ff7f00', '#377eb8'])
                       
    plt.subplot(2, 1, 1)
    plt.title('Training')

    plt.scatter(XY_train[:, 0], XY_train[:, 1], s=10, color=colors[(out_train + 1) // 2])

    plt.xlim(min_X, max_X)
    plt.ylim(min_Y, max_Y)
    plt.xticks(())
    plt.yticks(())
        
    plt.subplot(2, 1, 2)
    plt.title('Test')

    plt.scatter(XY_test[:, 0], XY_test[:, 1], s=10, color=colors[(out_test + 1) // 2])

    plt.xlim(min_X, max_X)
    plt.ylim(min_Y, max_Y)
    plt.xticks(())
    plt.yticks(())
        
    plt.savefig(os.path.join(results_path, basefile + "_outliers_plot.svg"), format = "svg");
    
    
    mask = np.logical_not(out_train);
    n_ac = np.sum(y_train == 1);
    n_nac = np.sum(y_train == 0);
    n_o_ac = np.sum(y_train[mask] == 1);
    n_o_nac = np.sum(y_train[mask] == 0);
    out_ac = np.float32(n_o_ac) / n_ac;
    out_nac = np.float32(n_o_nac) / n_nac;
    fdb_o = np.sum(out_test == False);
    fdb_c = out_test.shape[0];
    
    
    
    
    
    with open(os.path.join(results_path, basefile + "_outliers_stats.csv"), 'w') as fout:
        fout.write(','.join(['Train AC outliers', 
                             'Train NAC outliers', 
                             'Train AC count', 
                             'Train NAC count', 
                             '%% Train AC outliers', 
                             '%% Train NAC outliers',
                             'Test outliers',
                             'Test count',
                             'Test %% outliers',                             
                             ]));
    
        fout.write('\n');

        fout.write(','.join(['%s'%n_o_ac, 
                             '%s'%n_o_nac, 
                             '%s'%n_ac, 
                             '%s'%n_nac, 
                             '%s'%out_ac, 
                             '%s'%out_nac,
                             '%s'%fdb_o,
                             '%s'%fdb_c,
                             '%s'%(float(fdb_o)/fdb_c),
                             ]));
    
        fout.write('\n');
    
    outliers_list(os.path.join(results_path, basefile + "_outliers_train.csv"), train_ids[mask], get_list_by_mask(train_names, mask), get_list_by_mask(train_DBIDs, mask), get_list_by_mask(train_InChIKeys, mask));
    
    mask = np.logical_not(out_test);
    outliers_list(os.path.join(results_path, basefile + "_outliers_test.csv"), test_ids[mask], get_list_by_mask(test_names, mask), get_list_by_mask(test_DBIDs, mask), get_list_by_mask(test_InChIKeys, mask));
    
    X_train = X_train[out_train, :];
    y_train = y_train[out_train];
    train_ids = train_ids[out_train];
    train_names = get_list_by_mask(train_names, out_train);
    train_DBIDs = get_list_by_mask(train_DBIDs, out_train);
    train_InChIKeys = get_list_by_mask(train_InChIKeys, out_train);
    
    X_test = X_test[out_test, :];
    test_ids = test_ids[out_test];
    test_names = get_list_by_mask(test_names, out_test);
    test_DBIDs = get_list_by_mask(test_DBIDs, out_test);
    test_InChIKeys = get_list_by_mask(test_InChIKeys, out_test);
    
    return (X_train, y_train, train_ids, train_names, train_DBIDs, train_InChIKeys,
            X_test, test_ids, test_names, test_DBIDs, test_InChIKeys);

class PipelineClassifier(Pipeline):
    _estimator_type = "classifier";
            

def get_cv_prob_prediction_MMC(X, y, N_repeats, cv_folds, test_size, pipe, scoring, parameters, report_file):
    printlog('MMC based probability prediction...');
    pipe = PipelineClassifier(pipe);
    pipe._estimator_type = "classifier";
    param_grid = [parameters];

    return_prob = np.full(y.shape, -1.0, dtype = np.float32);
    
    scores = {};
    skf = StratifiedKFold(n_splits = N_repeats, random_state = None, shuffle = True);
    for train_index, test_index in skf.split(X, y):
        X_train = X[train_index, :];
        X_test  = X[test_index, :];
        y_train = y[train_index];
        y_test  = y[test_index];
        
        printlog('X_train: %s'%str(X_train.shape))
        printlog('y_train: %s'%str(y_train.shape))
        printlog('X_test: %s'%str(X_test.shape))
        printlog('y_test: %s'%str(y_test.shape))
        
        printlog('Npos: %s Nneg: %s'%(np.sum(y_train == 1), np.sum(y_train == 0)))
        
        grid = GridSearchCV(pipe, param_grid, cv = cv_folds, scoring = scoring, verbose = 2, refit = 'balance')
        grid.fit(X_train, y_train)
        
        printlog("Best cross-validation score: {:.2f}".format(grid.best_score_))
        test_score = grid.score(X_test, y_test);
        printlog("Test-set score: {:.2f}".format(test_score))
        
        for key in scoring:
            if key in scores:
                scores[key].append(scoring[key](grid, X_test, y_test));
            else:
                scores[key] = [scoring[key](grid, X_test, y_test)];
        
        return_prob[test_index] = get_class_prob(grid, grid.predict_proba(X_test), 1);
    
    with open(report_file, 'w') as fout:
        for key in scores:
            values = np.array(scores[key]);
            fout.write('%s: %.2f (%.2f)\n'%(key, np.mean(values), np.std(values)));

    return return_prob;



def optimize_SVM(X, y, N_repeats, cv_folds, test_size, pipe, scoring, parameters, report_file):
    printlog('Optimizing parameters of SVM...');
    pipe = PipelineClassifier(pipe);
    
    param_grid = [parameters];

    return_prob = np.full(y.shape, -1.0, dtype = np.float32);
    
    best_params = {};
    scores = {};
    skf = StratifiedKFold(n_splits = N_repeats, random_state = None, shuffle = True);
    for train_index, test_index in skf.split(X, y):
        X_train = X[train_index, :];
        X_test  = X[test_index, :];
        y_train = y[train_index];
        y_test  = y[test_index];
        
        printlog('X_train: %s'%str(X_train.shape))
        printlog('y_train: %s'%str(y_train.shape))
        printlog('X_test: %s'%str(X_test.shape))
        printlog('y_test: %s'%str(y_test.shape))
        
        grid = GridSearchCV(pipe, param_grid, cv = cv_folds, scoring = scoring, verbose = 2, refit = 'balance')
        grid.fit(X_train, y_train)
        
        printlog("Best params:\n{}\n".format(grid.best_params_))
        printlog("Best cross-validation score: {:.2f}".format(grid.best_score_))
        test_score = grid.score(X_test, y_test);
        printlog("Test-set score: {:.2f}".format(test_score))
        
        for key in scoring:
            if key in scores:
                scores[key].append(scoring[key](grid, X_test, y_test));
            else:
                scores[key] = [scoring[key](grid, X_test, y_test)];
        
        for key in grid.best_params_:
            if key in best_params:
                best_params[key].append(grid.best_params_[key]);
            else:
                best_params[key] = [grid.best_params_[key]];
    
        return_prob[test_index] = get_class_prob(grid, grid.predict_proba(X_test), 1);
    
    printlog(str(best_params));
    
    with open(report_file, 'w') as fout:
        for key in scores:
            values = np.array(scores[key]);
            fout.write('%s: %.2f (%.2f)\n'%(key, np.mean(values), np.std(values)));
        for key in best_params:
            values = np.array(best_params[key]);
            fout.write('%s: %.2f (%.2f)\n'%(key, np.mean(values), np.std(values)));

    if 'classifier__gamma' in parameters:
        return np.mean(np.array(best_params['classifier__C'])), np.mean(np.array(best_params['classifier__gamma'])), return_prob;
    else:
        return np.mean(np.array(best_params['classifier__C'])), return_prob;
        

def get_mean_best_C(X, y, N_repeats, cv_folds, test_size, pipe, scoring, parameters, report_file):
        
   parameters['classifier__C'] = [10 ** -7, 
                                  10 ** -6, 
                                  10 ** -5, 
                                  10 ** -4, 
                                  10 ** -3, 
                                  10 ** -2, 
                                  10 ** -1, 
                                  10 ** 0, 
                                  10 ** 1, 
                                  ];
   C, return_prob = optimize_SVM(X, y, N_repeats, cv_folds, test_size, pipe, scoring, parameters, report_file);
   printlog('Final C value: %s'%C);
   return C, return_prob
    

def get_mean_best_C_and_gamma(X, y, N_repeats, cv_folds, test_size, pipe, scoring, parameters, report_file):
    n_features = float(min(X.shape[1], X.shape[0]));
    gamma_base = 1.0/n_features;
    gamma_startlog = np.log(gamma_base)/np.log(3.0);
    parameters['classifier__C'] = [10 ** -2, 
                                   10 ** -1, 
                                   10 ** 0, 
                                   10 ** 1, 
                                   10 ** 2, 
                                   ];

    parameters['classifier__gamma'] = [
                           3 ** (gamma_startlog -6), 
                           3 ** (gamma_startlog -5), 
                           3 ** (gamma_startlog -4), 
                           3 ** (gamma_startlog -3), 
                           3 ** (gamma_startlog -2), 
                           3 ** (gamma_startlog -1), 
                           3 ** (gamma_startlog), 
                           ];
            
    C, gamma, return_prob = optimize_SVM(X, y, N_repeats, cv_folds, test_size, pipe, scoring, parameters, report_file);        
    printlog('Final C and gamma: %s and %s'%(C, gamma));
    return C, gamma, return_prob
    
            
def optimize_classifier(X, y, method, N_repeats = 10, cv_folds = 5, test_size = 0.2, report_file = 'report.csv'):
    pipe = [('preprocessing', StandardScaler()),];

    parameters = {};
    
    printlog('Optimizing paramteres....')

    scoring_measure_balance = make_scorer(class_balancer, greater_is_better = True)
    scoring_measure_ac = make_scorer(ac_correct, greater_is_better = True)
    scoring_measure_nac = make_scorer(nac_correct, greater_is_better = True)
    scoring_measure_logratio = make_scorer(balance_log_ratio, greater_is_better = False)
    scoring = {'balance':scoring_measure_balance, 'ac_correct':scoring_measure_ac, 'nac_correct':scoring_measure_nac, 'logratio':scoring_measure_logratio};
            
    if method == 0: #Linear SVM - optimize C only
        pipe.append(('compression', PCA()));
        pipe.append(('classifier', LinearSVC_with_probability(class_weight = 'balanced')));
        best_C, return_prob = get_mean_best_C(X, y, N_repeats, cv_folds, test_size, pipe, scoring, parameters, report_file);
        parameters['classifier__C'] = [best_C];
    
    elif method == 1: #Radial SVM - optimize C and gamma
        pipe.append(('compression', PCA()));
        pipe.append(('classifier', SVC(class_weight = 'balanced', probability = True)));
        best_C, best_gamma, return_prob = get_mean_best_C_and_gamma(X, y, N_repeats, cv_folds, test_size, pipe, scoring, parameters, report_file);
        parameters['classifier__C'] = [best_C];
        parameters['classifier__gamma'] = [best_gamma];
        
    else: #MMC is parameter-less classifier
        pipe.append(('classifier', MMC_Classifier_PCA(class_weight = 'balanced')));
        return_prob = get_cv_prob_prediction_MMC(X, y, N_repeats, cv_folds, test_size, pipe, scoring, parameters, report_file);
        
    printlog('Obtaining performance of the best parameters...');
    pipe = PipelineClassifier(pipe);
    param_grid = [parameters];
    
    grid = GridSearchCV(pipe, param_grid, cv = N_repeats, scoring = scoring, verbose = 2, refit = 'balance');
    grid.fit(X, y);
    
    gene_scores = get_gene_scores(method, grid, X, y);
    
    return grid, return_prob, gene_scores;
























            