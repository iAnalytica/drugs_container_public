# -*- coding: utf-8 -*-
"""
DRUGS Project module: base64_encoder.py

This module performs corrected base64 encoding/decoding. The original python version
of base64 encoding had some issues with byte values higher than 127, thus this module
was created. It is used in this container to keep in line with the specifications of the
current DreamLab architecture for which the main propagation algorithm in C++ was tuned
(i.e. the data exchange had to be in text format).  

--------------------------------------------------------------------------

Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2019 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
__base64encoderrecord='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

__b64symbol_to_index = {};

for i in range(len(__base64encoderrecord)):
    __b64symbol_to_index[__base64encoderrecord[i]] = i;


def __decodequad(dquad):
    a=dquad[0];
    b=dquad[1];
    c=dquad[2];
    d=dquad[3];
    i=(a<<2)|((b & 0b00110000 ) >>4);
    j=((b & 0b00001111) <<4) | ((c & 0b00111100)>>2);
    k=((c & 0b00000011)<<6) | ((d & 0b00111111));
    return bytearray([i,j,k]);
        
def __convtripl(tripl):
    i=tripl[0];
    j=tripl[1];
    k=tripl[2];
    a=(i & 0b11111100)>>2;
    b=((i & 0b00000011)<<4) | ((j & 0b11110000) >>4);
    c=((j & 0b00001111) <<2) | ((k & 0b11000000) >>6);
    d=((k & 0b00111111));
    return __base64encoderrecord[a]+__base64encoderrecord[b]+\
    __base64encoderrecord[c]+__base64encoderrecord[d];
    
def encode_to_base64(btarray):
    '''
    EncodeToBase64(btarray: bytearray):string;
    - Converts bytearray input into Base64 encoded string, padding as necessary;
    '''
    bb=bytearray(btarray);
    j=len(bb)//3;
    k=len(bb)%3;
    encstring='';
    for i in range(0,j):
        tripl=bb[i*3:i*3+3];
        ss=__convtripl(tripl);
        encstring=encstring+ss;
    
    if k==1:
        tripl=bytearray([bb[j*3],0,0]);
        ss=__convtripl(tripl);
        encstring=encstring+ss[0:2];
        encstring=encstring+'==';
    elif k==2:
        tripl=bytearray([bb[j*3],bb[j*3+1],0]);
        ss=__convtripl(tripl);
        encstring=encstring+ss[0:3];
        encstring=encstring+'=';        
    
    return encstring;
    

def decode_from_base64(encstring):
    '''
    DecodeFromBase64(encstring:string):bytearray

    Decodes Base64 encoded string into bytearray. String length must be of 
    multiple of 4 and it should only contain the following symbols:
    
    ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=

    with '=' indicating the padding at the end of the string only.
    '''
    if len(encstring)%4!=0:
        raise NameError('String length not divisible by 4!');
    if len(encstring)==0:
        return bytearray();
    bb=bytearray();
    q=len(encstring)//4;
    try:
        for i in range(0,len(encstring)):
            bb.append(__b64symbol_to_index[encstring[i]]);
    except: 
        raise NameError('Non-standard characters in Base64 string!');
    
        
    decbb=bytearray();
    
    for i in range(0, q):
        dquad=bb[i*4:i*4+4];
        qq=__decodequad(dquad);
        decbb.extend(qq);
    if encstring[len(encstring)-1]=='=':
        decbb.pop();
    if encstring[len(encstring)-2]=='=':
        decbb.pop();
    
    return decbb;
    

    
    